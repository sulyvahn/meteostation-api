#! venv/flask/bin/python3
from flask import Flask, jsonify, request, abort, render_template
from werkzeug.exceptions import HTTPException

from weathersql import WeatherSQL
import weathertoken as tokens


app = Flask(__name__)
db = WeatherSQL(app)
app.static_folder = 'static'

def hex_to_int(num):
    return int(num, 16)


@app.route('/api/weather', methods=['GET'])
def get_last_data_all(): 
    result = db.fetch_last_weather(request.args.get('old'))
    return jsonify(result)


@app.route('/api/weather/<station_id>', methods=['GET', 'POST'])
def get_weather_method(station_id):
    station_id = hex_to_int(station_id)
    if request.method == 'GET':
        pos = request.args.get('pos')
        num = request.args.get('num')
        result = db.fetch_all_weather(station_id, pos, num)
    elif request.method == 'POST':
        station_token = request.json['station_token'] 
        if not tokens.ensure_station(station_token, station_id):
            abort(401)
        
        db.send_data(station_id, request.json['data'])
        result = {'code':200} 
    return jsonify(result)


@app.route('/api/stations', methods=['POST', 'GET'])
def get_station_method():
    if request.method == 'POST':
        result = db.add_station()
        result['station_token'] = tokens.gen_station_token(result['id'])
    elif request.method == 'GET':
        result = db.fetch_stations(request.args.get('old'))
    return jsonify(result)


@app.route('/api/stations/<station_id>', methods=['POST'])
def place_station(station_id):
    station_id = hex_to_int(station_id)

    user_token = request.json['user_token'] 
    if not tokens.ensure_user(user_token, station_id):
        abort(401)

    db.set_station_location(station_id, request.json['coords'])
    result = {'code':200}
    return jsonify(result)


@app.route('/api/users', methods=['GET'])
def get_user_token():
    station_id = request.args.get('station_id')
    if not station_id:
        abort(404)
    
    station_id = hex_to_int(station_id)
    result= {'user_token':tokens.gen_user_token(station_id)}
    return jsonify(result)


@app.errorhandler(Exception)
def handle_error(e):
    code = 500
    if isinstance(e, HTTPException):
        code = e.code
    return jsonify({'code': code}), code


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/gen_qr')
def qr():
    return render_template('qr.html')


if __name__ == '__main__':
    app.run(debug=False)