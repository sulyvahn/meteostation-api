var qrs_count = 0;

function print_QR() {
    //let prtContent = document.querySelector('.qr__cont');
    //let WinPrint = window.open('', '', 'left=0,top=0,width=800,height=900,toolbar=0,scrollbars=0,status=0');
    //WinPrint.document.write('<!DOCTYPE html><html><head><link rel="stylesheet" href="css/qr.css" type="text/css" /></head>')
    //WinPrint.document.write('<body>');
    //WinPrint.document.write(prtContent.innerHTML);
    //WinPrint.document.write('</body>');
    //WinPrint.document.close();
    //WinPrint.focus();
    //WinPrint.print();
    //WinPrint.close();

    document.querySelector('.flex-cont').classList.add('invisible');
    document.querySelector('.print__button').classList.add('invisible');
    window.print();
    document.querySelector('.flex-cont').classList.remove('invisible');
    document.querySelector('.print__button').classList.remove('invisible');
}

function gen_QR() {
    let station_id = document.getElementsByName('id')[0].value;
    fetch(`http://185.105.88.149/api/users?station_id=${station_id}`)
    .then(response => {
        return response.json();
    })
    .then(data => {
        if (qrs_count < 6) {
            document.querySelector('.qr__cont').classList.remove('invisible');
            document.querySelector('.print__button').classList.remove('invisible');
            data['station_id'] = station_id;
            let qrcode = new QRCode(document.querySelectorAll('.qrcode')[qrs_count], JSON.stringify(data));
            document.querySelectorAll('.qr__title')[qrs_count].innerHTML = station_id;
            qrs_count++;
        } 
    })
}