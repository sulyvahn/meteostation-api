mapboxgl.accessToken = 'pk.' +
    'eyJ1Ijoic29sYXJlY2xpcHNlIiwiYSI6ImNsNTBweHkwYTByN3gzY3F1N2N4NXRyb3kifQ.' +
    'tEjt1tRJnhTbQw76SAU4Jg';

const MAP = new mapboxgl.Map({
    container: 'map',
    style: 'mapbox://styles/mapbox/streets-v11',
    center: [104.287640, 52.256041],
    zoom: 12
    });

const LAST_WEATHER = get_last_weather();

var current_marker;
var show_old;


function _to_hex(num) {
    result = num.toString(16);
    if (result.length < 2) {
        result = '0' + result;
    }
    return result.toUpperCase();
}


async function get_stations() {
    let response;
    if (show_old) {
        response = await fetch('http://185.105.88.149/api/stations?old=1');
    } else {
        response = await fetch('http://185.105.88.149/api/stations');
    }
    let data = await response.json();
    data.forEach(station => {
        create_marker(station);
    });
}

async function get_last_weather() {
    let response;
    if (show_old) {
        response = await fetch('http://185.105.88.149/api/weather?old=1');
    } else {
        response = await fetch('http://185.105.88.149/api/weather');
    }
    
    let data = await response.json();
    return data;
}

function _clear_history() {
    document.querySelectorAll('.history__info').forEach(elem => elem.remove());   
    document.querySelector('.id').innerHTML = ''; 
}

async function show_history(id, pos) {
    _clear_history();
    let response;
    if (pos) {
        response = await fetch(`http://185.105.88.149/api/weather/${_to_hex(id)}?pos=${pos}`);
        document.querySelector('.id').innerHTML = `${_to_hex(id)}(${pos})`;
    }
    else {
        response = await fetch(`http://185.105.88.149/api/weather/${_to_hex(id)}`);
        document.querySelector('.id').innerHTML = _to_hex(id);
    }
    let data = await response.json();
    // creating elements
    data.forEach(dt => {
        let elem = document.createElement('div');
        elem.append(document.querySelector('.history__template').content.cloneNode(true));
        elem.classList.add('history__info');
        document.body.append(elem);
        document.querySelector('.history').append(elem);
        
        //adding hide/show toggle 
        let title = elem.querySelector('.history__datetime');
        title.onclick = () => {
            elem.querySelector('.metrics').classList.toggle('closed');
            title.classList.toggle('data_open');
            title.classList.toggle('data_closed');
        };

        //formatting date
        let f_dt = new Date(dt['dt']).toLocaleString('ru-RU', {timeZone:'Asia/Irkutsk', hour12: false});
        
        //inserting data
        elem.querySelector('.history__datetime').innerHTML = f_dt;
        elem.querySelector('.metrics__temp1').innerHTML = dt['temp1'];
        elem.querySelector('.metrics__temp2').innerHTML = dt['temp2'];
        elem.querySelector('.metrics__temp3').innerHTML = dt['temp3'];
        elem.querySelector('.metrics__pressure').innerHTML = dt['pressure'];
        elem.querySelector('.metrics__humidity').innerHTML = dt['humidity'];
    })
    
}

function change_marker_color(marker, active) {
    let marker_elem = marker.getElement();
    let color;
    if (active) {
        color = '#eb5146';
    }
    else {
        color = '#3FB1CE';
    }
    marker_elem
        .querySelector('path')
        .setAttribute("fill", color);
    marker._color = color; 
}

async function create_marker(station_data) {
    console.log(station_data['id']);
    let response;
    if (show_old) {
        response = await fetch('http://185.105.88.149/api/weather?old=1');
    } else {
        response = await fetch('http://185.105.88.149/api/weather');
    }
    let all_weather = await response.json();
    let weather = {};
    for (const _weather of all_weather) {
        if (_weather['station_id'] == station_data['id'] && 
            _weather['station_pos'] == station_data['pos']) {    
            weather = _weather;
            break;
        }  
    };
    let station_name = _to_hex(station_data['id']);
    if (show_old) {
        station_name += '(' + station_data['pos'] + ')'; 
    }
    let avg_temp = Math.round(((weather['temp1'] + weather['temp2'] + weather['temp3']) / 3) * 100) / 100; 
    if (!avg_temp) {
        avg_temp = 0;
        weather['humidity'] = 0;
        weather['pressure'] = 0;
    }
    let html = `<div>
    <label>Станция: </label> ${station_name}
    </div>
    <div>
    <label>Температура: </label> ${avg_temp}&#8451;
    </div>
    <div>
    <label>Влажность: </label> ${weather['humidity']}%
    </div>
    <div>
    <label>Давление: </label> ${weather['pressure']} Па
    </div>
    `;
    const popup = new mapboxgl.Popup({closeOnClick: false, closeButton: false})
    .setHTML(html);  

    const marker = new mapboxgl.Marker()
    .setLngLat([station_data['lat'],station_data['long']])
    .setPopup(popup)
    .addTo(MAP)
    .togglePopup();
    
    //show history
    if (show_old) {
        marker.getElement().addEventListener('click', () => 
            show_history(station_data['id'], station_data['pos']));
    } else {
        marker.getElement().addEventListener('click', () => 
            show_history(station_data['id']));
    }
    //always open
    marker.getElement().addEventListener('click', () => marker.togglePopup());
    //change color
    marker.getElement().addEventListener('click', () => {
        if (current_marker) {
            change_marker_color(current_marker, false);
        }
        change_marker_color(marker, true);
        current_marker = marker;
    });
    marker.getElement().id = station_data['id'];
    marker.getElement().classList.add('map__marker');
}


// show data from old positions
var checkbox = document.querySelector('.map__old');

checkbox.addEventListener('change', (event) => {
    if (event.currentTarget.checked) {
        show_old = true;
    }
    else {
        show_old = false;
    }
    let markers = document.querySelectorAll('.map__marker');
    for (const marker of markers) {
        marker.remove();
    }
    _clear_history();
    get_stations();
});

// main
get_stations();