from flask_mysqldb import MySQL


class WeatherSQL:
    def __init__(self, app):
        app.config.from_pyfile('config.py')
        self.mysql = MySQL(app)


    # DECORATORS
    def _fetch(func):
        def wrapper(self, *args, **kwargs):
            cursor = self.mysql.connection.cursor()
            sql = func(*args, **kwargs)
            cursor.execute(sql)
            return cursor.fetchall()
        return wrapper


    def _proc(func):
        def wrapper(self, *args, **kwargs):
            cursor = self.mysql.connection.cursor()
            proc_name, proc_params = func(*args, **kwargs)
            cursor.callproc(proc_name, proc_params) # no OUT params support
            self.mysql.connection.commit()
        return wrapper


    # REGULAR METHODS
    @_fetch
    def fetch_last_weather(include_old=0):
        if not include_old:
            sql = '''SELECT last_dt, m.station_id, temp1, temp2, temp3, humidity, pressure
                FROM measurements AS m
                JOIN (SELECT MAX(dt) AS last_dt, station_id
                      FROM measurements
                      GROUP BY station_id) AS last_m 
                ON  (last_m.last_dt = m.dt AND last_m.station_id = m.station_id);
            '''
        else:
            sql = '''SELECT dt, m.station_id, m.station_pos, temp1, temp2, temp3, humidity, pressure 
                FROM measurements AS m
                JOIN (SELECT MAX(dt) AS last_dt, station_id, station_pos
                      FROM measurements
                      GROUP BY station_id, station_pos) AS last_m 
                ON  (last_m.last_dt = m.dt AND last_m.station_id = m.station_id AND last_m.station_pos = m.station_pos);    
            '''
        return sql


    @_fetch
    def fetch_all_weather(station_id, station_pos=0, number=10):
        number = number or 100
        if not station_pos:
            sql = f'''SELECT dt, temp1, temp2, temp3, humidity, pressure
                FROM measurements AS m
                LEFT JOIN stations AS st 
                ON (station_id = id AND station_pos < pos) 
                WHERE pos IS NULL AND station_id = {station_id}
                ORDER BY dt DESC
                LIMIT {number}'''
        else:
            sql = f'''SELECT dt, temp1, temp2, temp3, humidity, pressure
            FROM measurements
            WHERE station_pos = {station_pos} AND station_id = {station_id}
            ORDER BY dt DESC
            LIMIT {number}'''
        return sql

    
    @_fetch
    def fetch_stations(include_old=0):
        if not include_old:
            sql = '''SELECT st1.id AS id, ST_X(st1.coords) AS `long`, ST_Y(st1.coords) AS `lat` 
                FROM stations AS st1
                LEFT JOIN stations AS st2 
                ON (st1.id = st2.id AND st1.pos < st2.pos) 
                WHERE st2.pos IS NULL'''
        else:
            sql = '''SELECT id AS id, pos, ST_X(coords) AS `long`, ST_Y(coords) AS `lat` FROM stations'''
        return sql


    @_proc
    def set_station_location(station_id, coords):
        return 'place_station', [station_id, f"POINT({coords['lat']} {coords['long']})"]

    
    @_proc
    def send_data(id, data):
        data = (
            id, 
            data['temp1'],
            data['temp2'],
            data['temp3'],
            data['humidity'],
            data['pressure'])
        return 'add_measurements', data
        

    def add_station(self):
        cursor = self.mysql.connection.cursor()
        cursor.execute('''SELECT LPAD(HEX(add_station()),2,'0') AS id''')
        self.mysql.connection.commit()
        return cursor.fetchone()