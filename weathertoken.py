import jwt


st_key = 'secret_station'
u_key = 'secret_user'


def _gen_by_id(id, key):
    token = jwt.encode({'id':id}, key)
    return token


def _ensure_by_id(token, id, key):
    try:
        decrypted = jwt.decode(token, key, algorithms=['HS256'])
    except(jwt.exceptions.InvalidSignatureError):
        return False
    
    if int(decrypted['id']) == id:
        result = True
    else:
        result = False
    return result


def gen_user_token(id):
    return _gen_by_id(id, u_key)


def gen_station_token(id=0):
    return _gen_by_id(id, st_key)


def ensure_user(token, id):
    return _ensure_by_id(token, id, u_key)


def ensure_station(token, id):
    return _ensure_by_id(token, id, st_key)
